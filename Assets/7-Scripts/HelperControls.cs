using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

[System.Serializable]
public class HelperConvo{
    [TextArea] public List<string> linesText = new List<string>();
    public List<AudioClip> linesAudio = new List<AudioClip>();
}


public class HelperControls : MonoBehaviour
{
    public static HelperControls Instance {get;set;}


    [SerializeField] GameObject charPrefab, canvasBlocker, resumeButton, quitButton;
    [SerializeField] Text defaultTextArea;
    [SerializeField] HelperConvo quittingPanic, quittingSad, quittingMad, quittingRevenge;
    [SerializeField] AudioSource voice;

    int placeInConvo = 0;

    public bool inConvo=false;
    public bool tryingToQuit=false;

    Text currentTextArea;
    HelperConvo currentConvo;
    GameObject currentChar;
    GameObject currentTextGO;
    CinemachineVirtualCamera currentCam;


    float timeBetweenMessages = 1.0f;
    bool waitingForNextMessage = false;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }

    void Update(){
        if(!inConvo){
            return;
        }
        if(Input.GetMouseButtonDown(0)){
            NextMessage();
        }
        
    }

    public void PopIntoPlace(Transform pos, HelperConvo convo, GameObject thisPrefab, CinemachineVirtualCamera vcam, Text textArea, GameObject textGO){ 
        // particle effts
        placeInConvo = 0;
        inConvo=true;
        currentConvo = convo;
        currentTextArea = textArea;
        currentTextGO = textGO;
        currentCam = vcam;
        GameObject newChar = Instantiate(thisPrefab, pos);
        currentChar = newChar;
        vcam.enabled=true;
        MenuControls.Instance.PausePlayer();
        textGO.SetActive(true);
        textArea.text = convo.linesText[placeInConvo];
        voice.clip = convo.linesAudio[placeInConvo];
        voice.Play();


        // play audio
        
    }

    public void NextMessage(){
        if(waitingForNextMessage){
            return;
        }
        placeInConvo++;
        if(placeInConvo>=currentConvo.linesText.Count){
            EndConvo();
            return;
        }
        currentTextArea.text = currentConvo.linesText[placeInConvo];
        voice.clip = currentConvo.linesAudio[placeInConvo];
        voice.Play();
        StartCoroutine(Waiting());
    }

    public void EndConvo(){
        inConvo=false;
        
        
        if(tryingToQuit){
            tryingToQuit = false;
            canvasBlocker.SetActive(false);
            charPrefab.SetActive(false);
            resumeButton.SetActive(true);
            quitButton.SetActive(true);
            MenuControls.Instance.HidePauseScreen();
        } else {
            Destroy(currentChar);
            currentCam.enabled = false;
            currentTextGO.SetActive(false);
            MenuControls.Instance.ResumePlayer();
        }
    }
    


    public void CharOnQuit(){
        canvasBlocker.SetActive(true);
        resumeButton.SetActive(false);
        quitButton.SetActive(false);
        placeInConvo = 0;
        inConvo = true;
        currentTextArea = defaultTextArea;
        switch(QuitControls.Instance.currentState){
            case NPCState.Panic:
                currentConvo = quittingPanic;
                break;
            case NPCState.Sad:
                currentConvo = quittingSad;
                break;
            case NPCState.Mad:
                currentConvo = quittingMad;
                break;
            case NPCState.Revenge:
                currentConvo = quittingRevenge;
                break;
        }

        currentTextArea.text = currentConvo.linesText[placeInConvo];
        voice.clip = currentConvo.linesAudio[placeInConvo];
        voice.Play();

        tryingToQuit=true;
        charPrefab.SetActive(true);
    }


    IEnumerator Waiting(){
        waitingForNextMessage = true;
        yield return new WaitForSeconds(timeBetweenMessages);
        waitingForNextMessage = false;
    }

}
