using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NPCState {Happy, Sad, Panic, Mad, Revenge}

public class QuitControls : MonoBehaviour
{
    public static QuitControls Instance {get;set;}

    [SerializeField] GameObject quitConfirm1, quitConfirm2;
    [SerializeField] List<GameObject> quitButtons = new List<GameObject>();
    [SerializeField] AudioSource resumeSFX, quitSFX;

    public NPCState currentState = NPCState.Happy;
    float cooldown = 120.0f;

    Coroutine stateChange;

    int currentQuit = 0;
    bool finalQuitStage = false;

    public delegate void OnStateChange();
    public static OnStateChange changeNPCs;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }

    public void MovingQuit(){
        if(finalQuitStage){
            RealQuit();
            return;
        }
        if(currentQuit>=quitButtons.Count-1){
            QuitConfirm1();
            return;
        }
        quitSFX.Play();
        quitButtons[currentQuit].SetActive(false);
        currentQuit++;
        quitButtons[currentQuit].SetActive(true);
    }

    public void QuitConfirm1(){
        quitSFX.Play();
        quitConfirm1.SetActive(true);
    }
    
    public void QuitConfirm2(){
        quitSFX.Play();
        quitConfirm1.SetActive(false);
        quitConfirm2.SetActive(true);
    }


    public void ResetQuit(){
        quitConfirm1.SetActive(false);
        quitConfirm2.SetActive(false);
        for (int i=1; i<quitButtons.Count; i++){
            quitButtons[i].SetActive(false);
        }
        quitButtons[0].SetActive(true);
        currentQuit=0;
    }

    public void ChangeQuitState(){
        quitSFX.Play();
        switch(currentState){
            case NPCState.Happy:
                currentState = NPCState.Panic;
                break;
            case NPCState.Panic:
                currentState = NPCState.Sad;
                break;
            case NPCState.Sad:
                currentState = NPCState.Mad;
                break;
            case NPCState.Mad:
                currentState = NPCState.Revenge;
                finalQuitStage=true;
                break;
        }
        
        
        // MenuControls.Instance.HidePauseScreen();
        ResetQuit();
        HelperControls.Instance.CharOnQuit();
        changeNPCs();
        if(stateChange!=null){
            StopCoroutine(stateChange);
        }
        stateChange = StartCoroutine(StateCooldown());
    }


    void RealQuit(){
        quitSFX.Play();
        // TODO: Save player prefs here
        Application.Quit();
    }
    

    IEnumerator StateCooldown(){
        yield return new WaitForSeconds(cooldown);
        currentState = NPCState.Happy;
        
        changeNPCs();
    }

    public void ResumeSFX(){
        resumeSFX.Play();
    }

}
