using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Furniture : MonoBehaviour
{
    public int maxHits = 3;
    public int currentHits = 0;
    Coroutine Waiting;


    public void GetHit(){
        currentHits++;
        if(currentHits>=maxHits){
            StopCoroutine(Waiting);
            Destroy(this.gameObject);
        } else {
            Waiting = StartCoroutine(WaitToRepair());
        }
    }

    IEnumerator WaitToRepair(){
        yield return new WaitForSeconds(3.0f);
        currentHits = 0;
    }


}
