using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public NPCState currentState;

    [SerializeField] Renderer charRend;
    [SerializeField] Material happyMat, panicMat, sadMat, madMat, revengeMat;


    void OnEnable(){
        QuitControls.changeNPCs += ChangeState;
    }

    void ChangeState(){
        switch(QuitControls.Instance.currentState){
            case NPCState.Happy:
                StartHappy();
                break;

            case NPCState.Panic:
                StartPanic();
                break;
            
            case NPCState.Sad:
                StartSad();
                break;
                
            case NPCState.Mad:
                StartMad();
                break;

            case NPCState.Revenge:
                StartRevenge();
                break;
        }
    }

    void Start(){
        StartHappy();
    }


    void StartHappy(){
        currentState = NPCState.Happy;
        charRend.material = happyMat;
    }
    
    void StartPanic(){
        currentState = NPCState.Panic;
        charRend.material = panicMat;
    }

    void StartSad(){
        currentState = NPCState.Sad;
        charRend.material = sadMat;
    }

    void StartMad(){
        currentState = NPCState.Mad;
        charRend.material = madMat;
    }

    void StartRevenge(){
        currentState = NPCState.Revenge;
        charRend.material = revengeMat;
    }

    
    void OnDisable(){
        QuitControls.changeNPCs -= ChangeState;
    }
}
