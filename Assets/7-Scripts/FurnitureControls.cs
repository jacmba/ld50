using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class FurnitureItem{
    public string name;
    [TextArea] public string desc;
    public GameObject go;
    public Sprite sprite;
    public float cost;
}

public class FurnitureControls : MonoBehaviour
{
    public static FurnitureControls Instance {get;set;}

    [SerializeField] Transform placementParent;
    [SerializeField] AudioSource placeSFX, cancelSFX;
    public GameObject currentItem;
    int currentItemNum;

    
    public List<FurnitureItem> items = new List<FurnitureItem>();
    public List<int> ownedFurniture = new List<int>();

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }

    void Update(){
        if(currentItem!=null){
            if(Input.GetMouseButtonDown(0)){
                if(currentItem!=null){
                    PlaceGO();
                }
            } else if(Input.GetMouseButtonDown(1)){
                CancelItem();
            } else if(Input.GetKey(KeyCode.R)){
                float yRot = 0.1f;
                currentItem.transform.Rotate(0,yRot,0);
            }
        }
    }



    public void StartPlacing(int goNum){
        CursorControls.Instance.ShowPlacingInfo();
        PlayerControls.Instance.HideAxe();
        currentItemNum = goNum;
        GameObject itemPrefab = items[goNum].go;
        GameObject newItem = Instantiate(itemPrefab, placementParent);
        currentItem = newItem;
    }


    void PlaceGO(){
        placeSFX.Play();
        currentItem.transform.parent = null;
        currentItem.transform.position = placementParent.position;
        Collider col = currentItem.transform.GetComponent<Collider>();
        if(col!=null){
            col.enabled=true;
        }
        RemoveItem(currentItemNum);
        currentItem=null;
        
        PlayerControls.Instance.ShowAxe();
        CursorControls.Instance.HidePlacingInfo();
        
    }

    public void CancelItem(){
        cancelSFX.Play();
        Destroy(currentItem);
        PlayerControls.Instance.ShowAxe();
        CursorControls.Instance.HidePlacingInfo();
    }

    public void AddItem(int itemNum){
        ownedFurniture.Add(itemNum); 
    }

    public void RemoveItem(int itemNum){
        ownedFurniture.Remove(itemNum);
    }
}
