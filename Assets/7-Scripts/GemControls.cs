using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemControls : MonoBehaviour
{
    public static GemControls Instance {get;set;}

    [SerializeField] AudioSource pickupSFX, respawn;
   
    public float gemCount;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }

    public void AddGems(float numOfGems){
        gemCount+=numOfGems;
        HUDControls.Instance.UpdateHUD();
        pickupSFX.Play();
    }
    public void RemoveGems(float numOfGems){
        gemCount-=numOfGems;
        HUDControls.Instance.UpdateHUD();
    }

    public void PlayRespawnSound(){
        respawn.Play();
    }
}
