using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class MenuControls : MonoBehaviour
{
    public static MenuControls Instance {get;set;}


    [SerializeField] FirstPersonController fpc;
    public bool gameIsPaused = false;
    [SerializeField] GameObject menu, buttonPrefab;
    [SerializeField] Transform buttonsParent;
    [SerializeField] AudioSource select;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }

    void Update(){
        if(Input.GetKeyDown(KeyCode.Escape)){
            if(ShopControls.Instance.shopMenuIsOpen){
                ShopControls.Instance.CloseShopMenu();
            } if(FurnitureControls.Instance.currentItem!=null){
                FurnitureControls.Instance.CancelItem();
            } else if(gameIsPaused){
                HidePauseScreen();
            } else {
                ShowPauseScreen();
            }
        }
    }

    public void PausePlayer(){
        fpc.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void ResumePlayer(){
        fpc.enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void ShowPauseScreen(){
        select.Play();
        PlayerControls.Instance.HideAxe();
        PausePlayer();
        gameIsPaused = true;
        menu.SetActive(true);
        PopulateFurnitureList();
        // ShopControls.Instance.CloseShopMenuDontResume();
    }

    public void HidePauseScreen(){
        PlayerControls.Instance.ShowAxe();
        ResumePlayer();
        gameIsPaused = false;
        menu.SetActive(false);
        QuitControls.Instance.ResetQuit();
    }

    void PopulateFurnitureList(){
        foreach(Transform child in buttonsParent){
            Destroy(child.gameObject);
        }
        for(int i = 0; i<FurnitureControls.Instance.ownedFurniture.Count; i++){
            GameObject buttonGO = Instantiate(buttonPrefab, buttonsParent);
            buttonGO.GetComponent<ButtonInfo>().furnitureNum = FurnitureControls.Instance.ownedFurniture[i];
            buttonGO.transform.GetChild(1).GetComponent<Image>().sprite = FurnitureControls.Instance.items[FurnitureControls.Instance.ownedFurniture[i]].sprite;
            // say how many
            buttonGO.GetComponent<Button>().onClick.AddListener(()=>{
                select.Play();
                HidePauseScreen();
                FurnitureControls.Instance.StartPlacing(buttonGO.GetComponent<ButtonInfo>().furnitureNum);
            });
        }
    }

}
