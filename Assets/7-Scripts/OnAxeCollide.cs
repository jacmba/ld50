using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnAxeCollide : MonoBehaviour
{
    public static OnAxeCollide Instance {get;set;}
    [SerializeField] float axeDamageMin, axeDamageMax;
    Collider currentCol;
    public bool inSwing;
    [SerializeField] AudioSource axeSource;
    [SerializeField] List<AudioClip> axeHits = new List<AudioClip>();
    [SerializeField] AudioSource depositReady;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }

    public void PlayDepositDoneSound(){
        depositReady.Play();
    }

    void OnTriggerEnter(Collider col){
        if(col.gameObject.layer == LayerMask.NameToLayer("OreDeposit") || col.gameObject.layer == LayerMask.NameToLayer("Furniture")){
            currentCol = col;
            
            CheckForDamage();
        }
    }
    void OnTriggerExit(Collider col){
        if(col.gameObject.layer == LayerMask.NameToLayer("OreDeposit") || col.gameObject.layer == LayerMask.NameToLayer("Furniture")){
            currentCol = null;
        }
    }

    public void CheckForDamage(){
        if(currentCol==null){
            return;
        }
        
        Furniture furniture = currentCol.transform.GetComponent<Furniture>();

        OreDeposit ore = null;
        if(currentCol.transform.parent!=null){
            ore = currentCol.transform.parent.GetComponent<OreDeposit>();
        }
        

        if(ore!=null && inSwing){
            float axeDamage = Random.Range(axeDamageMin, axeDamageMax);
            ore.TakeDamage(axeDamage);
            int randSound = Random.Range(0,axeHits.Count);
            axeSource.clip = axeHits[randSound];
            axeSource.Play();
        } else if(furniture && inSwing){
            furniture.GetHit();
            int randSound = Random.Range(0,axeHits.Count);
            axeSource.clip = axeHits[randSound];
            axeSource.Play();
        }
    }

}
