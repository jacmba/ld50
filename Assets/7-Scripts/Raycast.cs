using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{
    float interactionDistance = 1.5f;
    [SerializeField] LayerMask layerMask;
    GameObject currentTarget;

    void Update(){
        if(HelperControls.Instance.inConvo){
            CursorControls.Instance.ShowText("NEXT");
            return;
        }
        if(MenuControls.Instance.gameIsPaused){
            return;
        }
        if(FurnitureControls.Instance.currentItem!=null){
            CursorControls.Instance.ShowText("PLACE");
            return;
        }
        RaycastHit hit;
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f,0.5f,0));
        
        if(Physics.Raycast(ray, out hit, interactionDistance, layerMask)){
            currentTarget = hit.transform.gameObject;
                Shop shop = currentTarget.GetComponent<Shop>();

            if(shop!=null){ 
                CursorControls.Instance.ShowText("VIEW SHOP");
                if(Input.GetMouseButtonDown(0)){
                    ShopControls.Instance.OpenShopMenu(shop);
                }
            } else if(currentTarget.gameObject.layer == LayerMask.NameToLayer("OreDeposit")){ 
                CursorControls.Instance.ShowText("MINE CRYSTALS");
            } else if(currentTarget.gameObject.layer == LayerMask.NameToLayer("Furniture")){ 
                CursorControls.Instance.ShowText("DESTROY");
            } else {
                CursorControls.Instance.DotOnly();
            }
            
        } else {
            CursorControls.Instance.DotOnly();
        }



    }
}
