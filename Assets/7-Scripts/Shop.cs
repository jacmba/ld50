using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public string shopName;
    public List<int> itemsForSale = new List<int>();
    public GameObject shopMenu;
    public Transform shopButtonParent;

}
